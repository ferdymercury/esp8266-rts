/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "NVS.h"

#include "nvs_flash.h"
#include "nvs.h"

namespace
{
	nvs_handle nvsHandle = 0;
}

namespace nvs
{
	void init()
	{
		esp_err_t ret = nvs_flash_init();
		if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
		{
			ESP_ERROR_CHECK(nvs_flash_erase());
			ret = nvs_flash_init();
		}
		ESP_ERROR_CHECK(ret);

		ESP_ERROR_CHECK(nvs_open("config", NVS_READWRITE, &nvsHandle));
	}

	void getStr(const char * key, char * value, size_t maxLen)
	{
		if (maxLen > 0)
		{
			size_t len = maxLen;
			nvs_get_str(nvsHandle, key, value, &len);

			// nvs_get_str probably does that but the doc is not explicit...
			value[maxLen - 1] = 0;
		}
	}

	void setStr(const char * key, char * value)
	{
		ESP_ERROR_CHECK(nvs_set_str(nvsHandle, key, value));
	}

	void get(const char * key, uint16_t & value)
	{
		nvs_get_u16(nvsHandle, key, &value);
	}

	void get(const char * key, int16_t & value)
	{
		nvs_get_i16(nvsHandle, key, &value);
	}

	void set(const char * key, uint16_t value)
	{
		ESP_ERROR_CHECK(nvs_set_u16(nvsHandle, key, value));
	}

	void set(const char * key, int16_t value)
	{
		ESP_ERROR_CHECK(nvs_set_i16(nvsHandle, key, value));
	}

	void commit()
	{
		ESP_ERROR_CHECK(nvs_commit(nvsHandle));
	}
}
