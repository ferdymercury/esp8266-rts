/*
 * Copyright 2020 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SimpleProto.h"

#include <atomic>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#include "../NVS.h"
#include "../radio/RTSFrame.h"
#include "../radio/RadioTask.h"

#include "NetworkImpl.h"
#include "TCPSocket.h"

namespace
{
	const char LOG_TAG[] = "SimpleProto";

	SemaphoreHandle_t configMutex;

	uint16_t port = CONFIG_DEFAULT_PORT;

	const char NVS_KEY_PORT[] = "port";

	// no trailing \0 in the replies
#if CONFIG_RADIO_DEBUG_WIFI_COMMANDS != 1
	const char REPLY_COMMAND_DISABLED[] =
	{
		'E', 'R', 'R', 'O', 'R', ':', ' ',
		'C', 'o', 'm', 'm', 'a', 'n', 'd', ' ', 'd', 'i', 's', 'a', 'b', 'l', 'e', 'd', ' ',
		'i', 'n', ' ', 'c', 'o', 'n', 'f', 'i', 'g', '.', '\n'
	};
#endif
	const char REPLY_OK[] = { 'O', 'K', '\n' };

	constexpr uint32_t makeUInt32LE(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3)
	{
		return (b3 << 24) | (b2 << 16) | (b1 << 8) | b0;
	}

	enum class Command: uint8_t
	{
		Ping,
		RTSCommand,
		RTSCommandContinuous,
		ResetRadio,
		ReadRegister,
		WriteRegister
	};

	struct NetworkRequestHeader
	{
		uint32_t magic;
		static constexpr uint32_t MAGIC = makeUInt32LE('!', 'R', 'T', 'S');

		uint8_t version;
		static constexpr uint8_t VERSION = 1;

		Command command;

		// TODO: signature or checksum
	} __attribute__((packed));

	struct NetworkRequestRTSCommand
	{
		uint8_t key;
		uint8_t ctrl;
		uint16_t rollingCode;
		uint32_t address;
	} __attribute__((packed));

	struct NetworkRequestReadRegister
	{
		uint8_t r;
	} __attribute__((packed));

	struct NetworkRequestWriteRegister
	{
		uint8_t r;
		uint8_t value;
	} __attribute__((packed));

	void readConfig()
	{
		nvs::get(NVS_KEY_PORT, port);
	}

	void handlePingCommand(wifi::TCPSocket & s)
	{
		const char PONG[] = { 'P', 'O', 'N', 'G', '\n' };

		ESP_LOGI(LOG_TAG, "%s", __func__);
		s.doWrite(PONG);
	}

	void handleRTSCommand(wifi::TCPSocket & s, bool continuous)
	{
		NetworkRequestRTSCommand command;
		if (s.doRead(command))
		{
			ESP_LOGI(LOG_TAG, "%s: key: 0x%02x, ctrl: 0x%02x, rolling code: 0x%04x, address: 0x%06x",
				__func__, command.key, command.ctrl, command.rollingCode, command.address);

			const RTSFrame frame =
			{
				command.key,
				static_cast<Action>(command.ctrl),
				command.rollingCode,
				command.address
			};

			if (continuous)
			{
				// continuous stream of packets until the socket is closed
				// TODO: improve this - use nonblocking socket API
				ESP_LOGI(LOG_TAG, "%s: starting continuous broadcast...", __func__);
				static std::atomic_bool keepGoing(false);

				keepGoing = true;
				radio::broadcastRTSFrameContinuous(frame, keepGoing);

				uint8_t dummy;
				while (s.doRead(dummy))
					;

				keepGoing = false;
				ESP_LOGI(LOG_TAG, "%s: continuous broadcast over", __func__);
			}
			else
			{
				// just a single RTS frame
				// TODO: this should just be blocking, no need for a separate radio task
				radio::broadcastRTSFrame(frame);
				ESP_LOGI(LOG_TAG, "%s: transmission requested", __func__);
			}
		}
		else
			ESP_LOGE(LOG_TAG, "%s: can't receive request payload", __func__);
	}

	void handleResetRadioCommand(wifi::TCPSocket & s)
	{
#if CONFIG_RADIO_DEBUG_WIFI_COMMANDS == 1
		ESP_LOGI(LOG_TAG, "%s: resetting radio", __func__);
		radio::resetRadio();
		s.doWrite(REPLY_OK);
#else
		ESP_LOGE(LOG_TAG, "%s: command disabled in config", __func__);
		s.doWrite(REPLY_COMMAND_DISABLED);
#endif
	}

	void handleReadRegisterCommand(wifi::TCPSocket & s)
	{
		NetworkRequestReadRegister command;
		if (s.doRead(command))
		{
#if CONFIG_RADIO_DEBUG_WIFI_COMMANDS == 1
			ESP_LOGI(LOG_TAG, "%s: register: 0x%02x", __func__, command.r);
			const uint8_t value = radio::readRegister(command.r);
			ESP_LOGI(LOG_TAG, "%s: value: 0x%02x", __func__, value);
			s.doWrite(REPLY_OK);
			s.doWrite(value);
#else
			ESP_LOGE(LOG_TAG, "%s: command disabled in config", __func__);
			s.doWrite(REPLY_COMMAND_DISABLED);
#endif
		}
		else
			ESP_LOGE(LOG_TAG, "%s: can't receive request payload", __func__);
	}

	void handleWriteRegisterCommand(wifi::TCPSocket & s)
	{
		NetworkRequestWriteRegister command;
		if (s.doRead(command))
		{
#if CONFIG_RADIO_DEBUG_WIFI_COMMANDS == 1
			ESP_LOGI(LOG_TAG, "%s: register: 0x%02x, value: 0x%02x", __func__, command.r, command.value);
			radio::writeRegister(command.r, command.value);
			s.doWrite(REPLY_OK);
#else
			ESP_LOGE(LOG_TAG, "%s: command disabled in config", __func__);
			s.doWrite(REPLY_COMMAND_DISABLED);
#endif
		}
		else
			ESP_LOGE(LOG_TAG, "%s: can't receive request payload", __func__);
	}

	extern "C" void simpleProtoTaskFn(void*)
	{
		ESP_LOGI(LOG_TAG, "%s: waiting for network", __func__);
		while (true)
		{
			xEventGroupWaitBits(wifi::wifiEventGroup, wifi::CONNECTED_BIT, false, true, portMAX_DELAY);
			ESP_LOGI(LOG_TAG, "%s: wifi connected", __func__);

			uint16_t p;
			{
				MutexGuard g(configMutex);
				p = port;
			}
			ESP_LOGI(LOG_TAG, "%s: listening on port %u\n", __func__, static_cast<unsigned>(p));

			wifi::TCPListeningSocket listeningSocket(p);
			while (wifi::isConnected())
			{
				wifi::TCPSocket s = listeningSocket.acceptConnection();

				NetworkRequestHeader requestHeader;
				if (s.doRead(requestHeader))
				{
					if (requestHeader.magic != NetworkRequestHeader::MAGIC)
					{
						ESP_LOGE(LOG_TAG, "%s: wrong command magic", __func__);
						continue;
					}

					if (requestHeader.version != NetworkRequestHeader::VERSION)
					{
						ESP_LOGE(LOG_TAG, "%s: wrong command version (got %u, expected %u)",
							__func__, requestHeader.version, NetworkRequestHeader::VERSION);
						continue;
					}

					switch (requestHeader.command)
					{
					case Command::Ping:
						handlePingCommand(s);
						break;
					case Command::RTSCommand:
						handleRTSCommand(s, false);
						break;
					case Command::RTSCommandContinuous:
						handleRTSCommand(s, true);
						break;
					case Command::ResetRadio:
						handleResetRadioCommand(s);
						break;
					case Command::ReadRegister:
						handleReadRegisterCommand(s);
						break;
					case Command::WriteRegister:
						handleWriteRegisterCommand(s);
						break;
					default:
						ESP_LOGE(LOG_TAG, "%s: unknown command %u", __func__, static_cast<uint8_t>(requestHeader.command));
						break;
					}
				}
				else
					ESP_LOGE(LOG_TAG, "%s: can't read from socket", __func__);

				ESP_LOGI(LOG_TAG, "%s: closing connection", __func__);
			}

			ESP_LOGI(LOG_TAG, "%s: wifi disconnected, waiting for reconnect", __func__);
		}
	}
}

namespace wifi
{
	void initSimpleProto()
	{
		configMutex = xSemaphoreCreateMutex();

		// read initial config
		readConfig();

		ESP_LOGI(LOG_TAG, "%s: listening on port %u", __func__, port);
	}

	void setSimpleProtoPort(uint16_t newPort)
	{
		{
			MutexGuard g(configMutex);

			port = newPort;
			nvs::set(NVS_KEY_PORT, port);
		}

		// TODO: to change this w/o reboot, I think the asynchronous lwip API would be needed...
		ESP_LOGI(LOG_TAG, "%s: port change will be applied after reboot", __func__);
	}

	void startSimpleProto(UBaseType_t priority)
	{
		xTaskCreate(simpleProtoTaskFn, "simpleProtoTask", 2*1024, nullptr, priority, nullptr);
	}
}
