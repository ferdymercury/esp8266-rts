/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_BIT_STREAM_H
#define ESP8266_RTS_BIT_STREAM_H

#include <cstdint>

class BitStream
{
public:
	template<size_t N>
	BitStream(uint8_t (&buffer)[N], unsigned bitOversampling):
		m_bitOversampling(bitOversampling),
		m_current(buffer),
		m_end(buffer + N),
		m_bitIndex(0)
	{}

	BitStream & operator<<(bool b)
	{
		for (unsigned i = 0; i < m_bitOversampling; i++)
			appendSymbol(b);

		return *this;
	}

	uint8_t * getCurrentEnd() const
	{
		uint8_t * end = m_current;
		if (m_bitIndex > 0)
			end++;
		return end;
	}

private:
	void appendSymbol(bool b)
	{
		if (m_current != m_end)
		{
			// zero every byte first
			if (m_bitIndex == 0)
				*m_current = 0;

			if (b)
				*m_current |= 1 << (7 - m_bitIndex);
			m_bitIndex++;

			if (m_bitIndex == 8)
			{
				m_current++;
				m_bitIndex = 0;
			}
		}
	}

	const uint8_t m_bitOversampling;
	uint8_t * m_current;
	uint8_t * m_end;
	unsigned m_bitIndex;
};

#endif // ESP8266_RTS_BIT_STREAM_H
