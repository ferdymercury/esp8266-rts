/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RadioTask.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#include "driver/gpio.h"
#include "driver/hw_timer.h"

#include <algorithm>
#include <type_traits>

#include "../Utils.h"
#include "BuffersReader.h"
#include "ManchesterEncoder.h"
#include "BitStream.h"
#include "RFM69.h"
#include "../NVS.h"

namespace
{
	const char LOG_TAG[] = "RadioTask";

	const char NVS_KEY_TX_POWER[] = "txPower";

	constexpr uint8_t TASK_NOTIFY_BIT_PACKET_SENT = 1;
	constexpr uint8_t TASK_NOTIFY_FIFO_LEVEL = 2;
	constexpr uint8_t TASK_NOTIFY_INTER_FRAME_GAP_PASSED = 3;

	/*
	* Calculated by tools/calc-rfm69-bitrate.py.
	*
	* This differs a bit from the time used in
	* https://pushstack.wordpress.com/2019/03/02/using-semtech-sx1231-to-transmit-custom-frames/
	* That is because the times I measure using my remote differ from that.
	* But using 9664 (302µs) (and regenerating the arrays below) works too.
	*/
	constexpr uint16_t RTS_BITRATE_COEFF = 10094; // 315.438µs

	// this matches the half-symbol duration used in tools/calc-rfm69-bitrate.py
	constexpr double HALFSYMBOL_DURATION = 645e-6;

	// produced by tools/rfm69-pulses-normal.sh
	constexpr uint8_t RTS_FRAME_HEADER_NORMAL[] =
	{
		0x01, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00, 0x01,
		0xfe, 0x01, 0xfe, 0x01, 0xff, 0xfc
	};

	// produced by tools/rfm69-pulses-repeat.sh
	constexpr uint8_t RTS_FRAME_HEADER_REPEAT[] =
	{
		0x01, 0xfe, 0x01, 0xfe, 0x01, 0xfe, 0x01, 0xfe,
		0x01, 0xfe, 0x01, 0xfe, 0x01, 0xfe, 0x01, 0xff,
		0xfc
	};

	constexpr double RTS_BIT_DURATION = RTS_BITRATE_COEFF / rfm69::FXOSC;
	constexpr unsigned BITS_PER_HALFSYMBOL = static_cast<unsigned>(HALFSYMBOL_DURATION / RTS_BIT_DURATION + 0.5f);
	constexpr unsigned RFM_STREAM_BITS = RTSFrame::SOMFY_FRAME_PAYLOAD_BYTES * 8 * 2 * BITS_PER_HALFSYMBOL; // 224 bits
	constexpr unsigned RFM_STREAM_BYTES = (RFM_STREAM_BITS + 7) / 8; // 28 bytes

	// gap between frames - when more than 1 frame is sent
	// with my remote it is ~27555µs
	constexpr double INTER_FRAME_GAP = 27555e-6;

	constexpr int DEFAULT_TX_POWER = 13;
	int txPower = DEFAULT_TX_POWER;

	TaskHandle_t radioTask;
	QueueHandle_t radioRequestQueue;
	SemaphoreHandle_t radioMutex;
	uint32_t interFrameGapTimer1Count;

	void taskNotifySetBitsFromISR(TaskHandle_t task, uint32_t bitsToSet)
	{
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		xTaskNotifyFromISR(task, bitsToSet, eSetBits, &xHigherPriorityTaskWoken);
		yieldFromISR(xHigherPriorityTaskWoken);
	}

	extern "C" void packetSentInterrupt(void*)
	{
		// kick off the inter-frame-gap timer
		// resetting the load count is not strictly necessary, but it feels safer
		hw_timer_set_load_data(interFrameGapTimer1Count);
		hw_timer_enable(true);

		taskNotifySetBitsFromISR(radioTask, BIT_u32(TASK_NOTIFY_BIT_PACKET_SENT));
	}

	extern "C" void fifoLevelInterrupt(void*)
	{
		taskNotifySetBitsFromISR(radioTask, BIT_u32(TASK_NOTIFY_FIFO_LEVEL));
	}

	void initRFM69()
	{
		// get version
		const uint8_t v = rfm69::readRegister(rfm69::RegVersion);

		if (v == 0 || v == 0xff)
		{
			// probably radio not connected or some other serious issue
			fatalError("Radio not connected?");
		}

		ESP_LOGI(LOG_TAG, "RFM69 version: rev %d, mask %d", v >> 4, v & 0xf);

		rfm69::setMode(rfm69::Mode::StandBy);

		/*
		* Set DIO mapping to useful values and disable ClkOut.
		* DIO5 is set to a value eother than 0 (CLKOUT), so perhaps setting the
		* CLKOUT frequency is not needed at all, but set it to OFF as recommended
		* in the manual.
		*
		* The ESP is only connected to DIO0, so that's the only important value.
		* The other DIO pins are set to values that might be useful for debugging.
		*/
		constexpr uint8_t DIO0 = 0b00; // PacketSent
		constexpr uint8_t DIO1 = 0b00; // FifoLevel
		constexpr uint8_t DIO2 = 0b00; // FifoNotEmpty
		constexpr uint8_t DIO3 = 0b00; // FifoFull
		constexpr uint8_t DIO4 = 0b01; // TxReady
		constexpr uint8_t DIO5 = 0b11; // ModeReady
		rfm69::writeRegister(rfm69::RegDioMapping1, (DIO0 << 6) | (DIO1 << 4) | (DIO2 << 2) | DIO3);
		rfm69::writeRegister(rfm69::RegDioMapping2, (DIO4 << 6) | (DIO5 << 4) | 0b111 /*disable CLKOUT*/);

		// set bitrate
		static_assert(RTS_BITRATE_COEFF > 0, "bitrate must be at least 1");
		rfm69::setBitRate(RTS_BITRATE_COEFF);

		// set frequency
		constexpr uint32_t RTS_FREQUENCY = 433460000; // 433.46 MHz
		constexpr uint32_t FREQ_COEFF = RTS_FREQUENCY / rfm69::FSTEP;
		rfm69::setFrequencyCoeff(FREQ_COEFF);

		// set packet mode with OOK and no shaping
		rfm69::writeRegister(rfm69::RegDataModul, 0b0001000);

		rfm69::setPreambleSize(0);

		// disable sync bytes: turn off SyncOn in RegSyncConfig
		rfm69::writeRegister(rfm69::RegSyncConfig, 0b0011000); // default value except for SyncOn which is OFF

		// setPacketConfig:
		// * fixed length (with length 0 this means unlimited format)
		// * DC free: off (we do all data preparation on our own)
		// * CrcOn: off
		// * CrcAutoClearOff: doesn't matter - RX only (use default: 1)
		// * AddressFiltering: doesn't matter - RX only (use default: 0)
		rfm69::writeRegister(rfm69::RegPacketConfig1, 0b00000000);

		// setPacketLength: 0 - together with fixed lenth means unlimited format
		rfm69::writeRegister(rfm69::RegPayloadLength, 0);

		// set TX start condition to FifoNotEmpty and threshold to
		rfm69::writeRegister(rfm69::RegFifoThresh, (1 << 7) | rfm69::FIFO_THRESHOLD);

		// necessary at least for the RFM69 "H" variants, as the default value means no TX at all
		rfm69::setTxPower(txPower);
	}

	void fillFifo(BuffersReader & reader, size_t minFreeFifoSize)
	{
		while (!reader.isAtEnd() && minFreeFifoSize > 0)
		{
			uint8_t const * chunk;

			// rfm69::writeRegisters can handle at most rfm69::MAX_WRITE_REGS_BYTES bytes :-(
			size_t chunkSize = std::min(minFreeFifoSize, rfm69::MAX_WRITE_REGS_BYTES);
			reader.getChunk(chunk, chunkSize);

			rfm69::writeRegisters(rfm69::RegFifo, chunk, chunkSize);
			minFreeFifoSize -= chunkSize;
		}
	}

	void stopFRC1()
	{
		hw_timer_enable(false); // FIXME: is it needed?
	}

	extern "C" void timerInterruptHandler(void *)
	{
		taskNotifySetBitsFromISR(radioTask, BIT_u32(TASK_NOTIFY_INTER_FRAME_GAP_PASSED));
	}

	bool waitForNotificationBit(uint8_t bit, TickType_t maxWait)
	{
		uint32_t notifyValue = 0;
		while ((notifyValue & BIT_u32(bit)) == 0)
		{
			// FIXME: decrease the tick count after each "wrong" interrupt
			if (!xTaskNotifyWait(0, BIT_u32(bit), &notifyValue, maxWait))
				return false;
		}

		return true;
	}

	template<typename F>
	class ScopedCleaner
	{
	public:
		ScopedCleaner(F && f):
			m_f(std::move(f))
		{}

		~ScopedCleaner()
		{
			m_f();
		}

	private:
		F m_f;
	};

	template<typename F>
	ScopedCleaner<F> makeScopedCleaner(F && f)
	{
		return ScopedCleaner<F>(std::move(f));
	}

	bool broadcastRFM69Frame(BuffersReader & reader)
	{
		// fill the FIFO before starting transmission
		fillFifo(reader, rfm69::FIFO_SIZE);

		// clear any unexpected notifications that might have been received
		xTaskNotifyStateClear(nullptr);

		// start transmitting
		rfm69::setMode(rfm69::Mode::Transmit);

		/*
		* Make sure that when the scope is left, radio is switched back to standby.
		* This seems to be necessary before another frame can be transmitted (e.g.
		* PacketSent seems to only be cleared when leaving TX mode.
		*/
		auto scopedCleaner = makeScopedCleaner([]{
			rfm69::setMode(rfm69::Mode::StandBy);
		});

		// while there is more data to be sent (but this should be always false as the frame is < FIFO_SIZE)
		while (!reader.isAtEnd())
		{
			if (!waitForNotificationBit(TASK_NOTIFY_FIFO_LEVEL, pdMS_TO_TICKS(1000)))
			{
				ESP_LOGE(LOG_TAG, "waiting for FifoLevel (RFM69 DIO1) timed out!");
				return false;
			}

			/*
			* Fill the FIFO.
			* We know the FIFO has at least FIFO_THRESHOLD bytes free now.
			*/
			fillFifo(reader, rfm69::FIFO_THRESHOLD);
		}

		if (!waitForNotificationBit(TASK_NOTIFY_BIT_PACKET_SENT, pdMS_TO_TICKS(1000)))
		{
			ESP_LOGE(LOG_TAG, "waiting for packet sent timed out!");
			return false;
		}

		return true;
	}

	template<typename TRepeatPredicate>
	bool broadcastRTSFrames(const uint8_t * payload, size_t payloadSize, TRepeatPredicate && repeatPredicate)
	{
		uint8_t const * buffers[2] =
		{
			RTS_FRAME_HEADER_NORMAL,
			payload
		};
		size_t sizes[2] =
		{
			size(RTS_FRAME_HEADER_NORMAL),
			payloadSize
		};

		// always broadcast the initial normal frame
		BuffersReader reader(buffers, sizes, size(buffers));
		auto frc1Stopper = makeScopedCleaner(stopFRC1); // to make sure the FRC1 timer is stopped before the scope is left
		if (!broadcastRFM69Frame(reader))
			return false;

		// optionally broadcast repeat frames
		unsigned repeatFramesCount = 0;
		if (repeatPredicate(repeatFramesCount))
		{
			buffers[0] = RTS_FRAME_HEADER_REPEAT;
			sizes[0] = size(RTS_FRAME_HEADER_REPEAT);

			while (repeatPredicate(repeatFramesCount))
			{
				if (!waitForNotificationBit(TASK_NOTIFY_INTER_FRAME_GAP_PASSED, pdMS_TO_TICKS(1000)))
				{
					ESP_LOGE(LOG_TAG, "waiting for InterFrameGap timer expired!");
					break;
				}

				if (!repeatPredicate(repeatFramesCount))
					break;

				reader = BuffersReader(buffers, sizes, size(buffers));
				if (!broadcastRFM69Frame(reader))
					return false;

				repeatFramesCount++;
			}
		}

		return true;
	}

	void resetRadio()
	{
		ESP_LOGI(LOG_TAG, "resetting radio...");
		rfm69::reset();
		initRFM69();
	}

	size_t makeRTSFrame(const RTSFrame & frame, uint8_t (&payload)[RFM_STREAM_BYTES])
	{
		// prepare frame payload, do manchester encoding and oversample
		uint8_t frameBytes[RTSFrame::SOMFY_FRAME_PAYLOAD_BYTES];
		frame.encode(frameBytes);

		BitStream bs(payload, BITS_PER_HALFSYMBOL);
		ManchesterEncoder<BitStream> encoder(bs);
		for (unsigned i = 0; i < size(frameBytes); i++)
			encoder << frameBytes[i];

		return bs.getCurrentEnd() - payload;
	}

	void broadcastRTSFrame(const RTSFrame & frame)
	{
		uint8_t payload[RFM_STREAM_BYTES];
		size_t size = makeRTSFrame(frame, payload);

		broadcastRTSFrames(payload, size, [](unsigned){
			return false; // no repeat frames
		});
	}

	void broadcastRTSFrameContinuous(const RTSFrame & frame, std::atomic_bool & repeat)
	{
		uint8_t payload[RFM_STREAM_BYTES];
		size_t size = makeRTSFrame(frame, payload);

		broadcastRTSFrames(payload, size, [&repeat](unsigned repeatFramesCount){
			return repeat.load();
		});
	}

	enum class RequestType: uint8_t
	{
		BroadcastRTSFrame,
		BroadcastRTSFrameContinuous,
		ResetRadio
	};

	struct Request
	{
		RequestType type;
		RTSFrame frame;
		std::atomic_bool * repeat;
	};
	static_assert(std::is_trivially_copyable<Request>::value, "queue item must be trivially copyable");

	extern "C" void radioTaskFn(void *)
	{
		rfm69::reset();
		initRFM69();

		// configure GPIO interrupts
		gpio_isr_handler_add(rfm69::DIO0_GPIO, packetSentInterrupt, nullptr);
		gpio_isr_handler_add(rfm69::DIO1_GPIO, fifoLevelInterrupt, nullptr);

		// TODO: should this be disabled for radio reset?
		gpio_set_intr_type(rfm69::DIO0_GPIO, GPIO_INTR_POSEDGE);
		gpio_set_intr_type(rfm69::DIO1_GPIO, GPIO_INTR_NEGEDGE);

		Request r;
		while (true)
		{
			if (xQueueReceive(radioRequestQueue, &r, portMAX_DELAY) != pdTRUE)
				fatalError("rfm69Transmit: can't receive from request queue");

			MutexGuard g(radioMutex);
			switch (r.type)
			{
			case RequestType::ResetRadio:
				resetRadio();
				break;

			case RequestType::BroadcastRTSFrame:
				broadcastRTSFrame(r.frame);
				break;

			case RequestType::BroadcastRTSFrameContinuous:
				broadcastRTSFrameContinuous(r.frame, *r.repeat);
				break;
			}
		}
	}

	// timer is used for measuring the inter-frame gap
	void initTimer()
	{
		hw_timer_init(timerInterruptHandler, nullptr);
		hw_timer_set_reload(false);
		constexpr hw_timer_clkdiv_t clkDiv = TIMER_CLKDIV_1;
		hw_timer_set_clkdiv(clkDiv);
		hw_timer_set_intr_type(TIMER_EDGE_INT);

		// tick frequency is 80 MHz, available divisors are: 1, 16 and 256
		// max. timer value is 2^32-1
		constexpr unsigned TICK_FREQ = 80000000;
		constexpr unsigned TIMER_FREQ = TICK_FREQ >> static_cast<int>(clkDiv);

		constexpr double INTER_FRAME_GAP_FREQ = 1.0f / INTER_FRAME_GAP; // ~ 36 Hz
		constexpr double TIMER_VAL = TIMER_FREQ / INTER_FRAME_GAP_FREQ;
		static_assert(TIMER_VAL > 0.0f, "timer value is <= 0");
		static_assert(TIMER_VAL < std::numeric_limits<uint32_t>::max(), "timer value does exceed max");

		interFrameGapTimer1Count = static_cast<uint32_t>(TIMER_VAL);
	}

	bool request(const Request & request)
	{
		constexpr TickType_t maxWait = pdMS_TO_TICKS(100);
		return xQueueSendToBack(radioRequestQueue, &request, maxWait) == pdTRUE;
	}
} // anonymous namespace

namespace radio
{
	void start(UBaseType_t priority)
	{
		// if no value was stored in NVS, p is not changed, so txPower is not changed
		int16_t p = txPower;
		nvs::get(NVS_KEY_TX_POWER, p);
		txPower = p;

		rfm69::initGPIO();
		rfm69::initSPI();
		initTimer();

		radioMutex = xSemaphoreCreateMutex();
		radioRequestQueue = xQueueCreate(5, sizeof(Request));
		if (radioRequestQueue == nullptr)
			fatalError("can't create rfm69 request queue");

		xTaskCreate(radioTaskFn, "radio", 2*1024, nullptr, priority, &radioTask);
	}

	bool resetRadio()
	{
		return request({
			RequestType::ResetRadio
		});
	}

	uint8_t readRegister(uint8_t r)
	{
		MutexGuard g(radioMutex);
		return rfm69::readRegister(r);
	}

	void writeRegister(uint8_t r, uint8_t value)
	{
		MutexGuard g(radioMutex);
		rfm69::writeRegister(r, value);
	}

	bool broadcastRTSFrame(const RTSFrame & frame)
	{
		return request({
			RequestType::BroadcastRTSFrame,
			frame,
			nullptr
		});
	}

	bool broadcastRTSFrameContinuous(const RTSFrame & frame, std::atomic_bool & repeat)
	{
		return request({
			RequestType::BroadcastRTSFrameContinuous,
			frame,
			&repeat
		});
	}

	bool setTxPower(int p)
	{
		MutexGuard g(radioMutex);
		if (rfm69::setTxPower(p))
		{
			txPower = p;
			nvs::set(NVS_KEY_TX_POWER, static_cast<int16_t>(p));
			return true;
		}

		return false;
	}

	int getTxPower()
	{
		MutexGuard g(radioMutex);
		return txPower;
	}
}
