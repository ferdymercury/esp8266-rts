/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SerialTask.h"

#include <cstdio>
#include <memory>
#include <cstring>
#include <algorithm>
#include <limits>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "driver/uart.h"
#include "linenoise/linenoise.h"

#include "../wifi/Network.h"
#include "../wifi/SimpleProto.h"
#include "../NVS.h"
#include "../radio/RFM69.h"
#include "../radio/RadioTask.h"
#include "../Utils.h"

namespace
{
	// A simple function that reads a line from the terminal without mixing
	// in the linenoise history and with optional no-echo.
	// This works in some x terminals, with some luck it works in your term too.
	void simpleReadLine(const char *prompt, char *str, size_t size, bool echo)
	{
		constexpr char BEEP = '\a';
		constexpr char BKSP = '\b';
		constexpr char ESC = 0x1b;

		if (size == 0)
			return;

		printf("%s", prompt);

		char * p = str;
		char * const end = str + size - 1;

		while (true)
		{
			int c = getc(stdin);

			if (c == ESC)
			{
				// escape sequence - just consume ETC '[' anything
				// this should also eat up arrows
				c = getc(stdin);
				if (c == '[')
				{
					getc(stdin);
				}
			}
			else if (c == BKSP || c == 127)
			{
				if (p > str)
				{
					p--;
					if (echo)
					{
						putc(BKSP, stdout);
						putc(' ', stdout);
						putc(BKSP, stdout);
					}
				}
				else
					putc(BEEP, stdout);
			}
			else if (c == '\n' || c == EOF)
			{
				// enter - always echoed to move the cursor to the next line
				putc(c, stdout);
				break;
			}
			else
			{
				if (p < end)
				{
					if (echo)
						putc(c, stdout);
					*p++ = c;
				}
				else
					putc(BEEP, stdout);
			}
		}

		*p = 0;
	}

	unsigned calculateMaskBits(const ip4_addr_t & mask)
	{
		// the IP address a.b.c.d is stored as (d<<24) | (c<<16) | (b<<8) | a
		unsigned idx = 0;
		while (idx < 32 && (((mask.addr >> idx) & 1) != 0))
			idx++;

		return idx;
	}

	extern "C" int statusCommand(int, char**)
	{
		wifi::WiFiStatus status;
		wifi::getStatus(status);

		printf("Radio TX power: %d dBm\n", radio::getTxPower());
		printf("Hostname template: '%s'\n", status.hostnameTemplate);
		printf("Hostname: '%s'\n", status.hostname);
		printf("SSID: '%s'\n", status.ssid);
		printf("Connected: %s\n", (status.connected ? "yes" : "no"));

		if (status.connected)
		{
			printf("BSSID: %02x:%02x:%02x:%02x:%02x:%02x\n", status.bssid[0], status.bssid[1], status.bssid[2],
				status.bssid[3], status.bssid[4], status.bssid[5]);
			printf("Channel: %u\n", status.channel);
			const tcpip_adapter_ip_info_t & ipv4cfg = status.ipv4Config;
			printf("IPv4 config: addr: %s/%u, gw: %s\n", ip4addr_ntoa(&ipv4cfg.ip),
				calculateMaskBits(ipv4cfg.netmask), ip4addr_ntoa(&ipv4cfg.gw));
			printf("IPv6 config: link local addr: %s\n", ip6addr_ntoa(&status.ipv6LinkLocalAddr));
		}

		return 0;
	}

	extern "C" int wifiCommand(int , char**)
	{
		char ssid[wifi::MAX_SSID_LEN];
		char password[wifi::MAX_PASSWORD_LEN];

		simpleReadLine("SSID? ", ssid, size(ssid), true);
		simpleReadLine("Password? ", password, size(password), false);

		wifi::setWiFiConfig(ssid, password);

		return 0;
	}

	int setHostname(const char *hostnameTemplate)
	{
		/*
		 * Do some basic sanity check: the hostname template is passed to snprintf
		 * which receives three ints - the last three bytes of the mac address.
		 * So it should not contain more than three '%' (not counting '%%'), and each
		 * should be of type x or u.
		 */
		const char * s = hostnameTemplate;
		unsigned fieldCount = 0;
		constexpr unsigned MAX_FIELD_COUNT = 3;

		while (*s && fieldCount <= MAX_FIELD_COUNT)
		{
			if (*s == '%')
			{
				s++;
				if (*s != '%')
				{
					// this is a bit too restrictive, but who would need all the fancy schmancy formatting options...
					while (isdigit(*s))
						s++;

					// now s should point at the conversion specifier (no length modifiers please...)
					if (*s != 'x' && *s != 'u')
					{
						printf("Use only conversion specifiers 'x' or 'u' to format the mac addess.\n");
						return 1;
					}

					fieldCount++;
				}
			}
			s++;
		}

		if (fieldCount > MAX_FIELD_COUNT)
		{
			printf("Too many printf fields (%%). At most %u is allowed.\n", MAX_FIELD_COUNT);
			return 1;
		}

		wifi::setHostnameTemplate(hostnameTemplate);
		return 0;
	}

	int setPort(const char *port)
	{
		unsigned p;
		if (sscanf(port, "%u", &p) != 1)
		{
			printf("Can't parse '%s' as a number.\n", port);
			return 1;
		}

		constexpr unsigned MAX = std::numeric_limits<uint16_t>::max();
		if (p > MAX)
		{
			printf("Port number out of range. Use a value in the range 0..%u\n", MAX);
			return 1;
		}

		wifi::setSimpleProtoPort(p);
		return 0;
	}

	int setTxPower(const char *power)
	{
		int p;
		if (sscanf(power, "%d", &p) != 1)
		{
			printf("Can't parse '%s' as a number.\n", power);
			return 1;
		}

		if (p < rfm69::MIN_TX_POWER || p > rfm69::MAX_TX_POWER)
		{
			printf("Invalid Tx power. Supported range is %d..%d.\n", rfm69::MIN_TX_POWER, rfm69::MAX_TX_POWER);
			return 1;
		}

		radio::setTxPower(p);

		return 0;
	}

	extern "C" int setCommand(int argc , char** argv)
	{
		using VarSetter = std::pair<const char *, int(*)(const char*)>;
		static const VarSetter VAR_SETTERS[] = {
			{ "hostname", setHostname },
			{ "port", setPort },
			{ "txPower", setTxPower }
		};

		if (argc != 3 || (argc == 3 && !strcmp(argv[1], "help")))
		{
			puts("Usage: set key value\n\nAvailable variables:");
			for (size_t i = 0; i < size(VAR_SETTERS); i++)
				puts(VAR_SETTERS[i].first);
		}
		else
		{
			const char * var = argv[1];
			const char * arg = argv[2];
			const auto & setter = std::find_if(std::begin(VAR_SETTERS), std::end(VAR_SETTERS), [var](const VarSetter& s)
			{
				return !strcmp(s.first, var);
			});

			if (setter != std::end(VAR_SETTERS))
				return (setter->second)(arg);
			else
			{
				printf("Unknown variable '%s'. Use '%s help' to list avalable variables.\n", var, argv[0]);
				return 1;
			}

		}
		return 0;
	}

	extern "C" int syncCommand(int, char**)
	{
		puts("Writing configuration to flash.");
		nvs::commit();

		return 0;
	}

	extern "C" int resetCommand(int, char**)
	{
		puts("Resetting...");
		esp_restart();
		return 0;
	}

	extern "C" int resetRadioCommand(int, char**)
	{
		puts("Resetting radio...");
		radio::resetRadio();
		return 0;
	}

	extern "C" int readRadioRegCommand(int argc , char** argv)
	{
		unsigned reg;
		if (argc != 2 || sscanf(argv[1], "%u", &reg) != 1)
		{
			puts("Usage: readRadioReg registerNumber");
			return 0;
		}

		if (reg >= 0x80)
		{
			puts("Register number out of range (allowed range: 0..127).");
			return 0;
		}

		const uint8_t value = radio::readRegister(static_cast<uint8_t>(reg));
		printf("RFM69 register %u (0x%02x): %u (0x%02x)\n", reg, reg, value, value);
		return 0;
	}

	extern "C" int writeRadioRegCommand(int argc , char** argv)
	{
		unsigned reg;
		unsigned value;
		if (argc != 3 || sscanf(argv[1], "%u", &reg) != 1 || sscanf(argv[2], "%u", &value) != 1)
		{
			puts("Usage: writeRadioReg registerNumber value");
			return 0;
		}

		if (reg >= 0x80)
		{
			puts("Register number out of range (allowed range: 0..127).");
			return 0;
		}

		if (value > 0xff)
		{
			puts("Value out of range (allowed range: 0..255).");
			return 0;
		}

		radio::writeRegister(static_cast<uint8_t>(reg), static_cast<uint8_t>(value));
		return 0;
	}

	void registerCommand(const char * command, const char * help, esp_console_cmd_func_t func)
	{
		esp_console_cmd_t cmd = {};
		cmd.command = command;
		cmd.help = help;
		cmd.func = func;

		ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
	}

	void registerCommands()
	{
		esp_console_register_help_command();

		registerCommand("status", "Show status of wifi.", statusCommand);
		registerCommand("wifi", "Configure wifi.", wifiCommand);
		registerCommand("set", "Set options.", setCommand);
		registerCommand("sync", "Write config to flash.", syncCommand);
		registerCommand("reset", "Reboot the board.", resetCommand);
		registerCommand("resetRadio", "Reset radio.", resetRadioCommand);
		registerCommand("readRadioReg", "Read radio register.", readRadioRegCommand);
		registerCommand("writeRadioReg", "Write radio register.", writeRadioRegCommand);
	}

	void initializeConsole()
	{
		esp_console_config_t console_config = {};
		console_config.max_cmdline_args = 8;
		console_config.max_cmdline_length = 256,
#if CONFIG_LOG_COLORS
		console_config.hint_color = atoi(LOG_COLOR_CYAN); // ... why is it defined to a string then?
#endif

		ESP_ERROR_CHECK(esp_console_init(&console_config));

		// connect linenoise with the esp_console
		linenoiseSetCompletionCallback(esp_console_get_completion);
		linenoiseSetHintsCallback((linenoiseHintsCallback*)esp_console_get_hint);

		// set command history size
		linenoiseHistorySetMaxLen(100);

		registerCommands();
	}

	extern "C" void serialTaskFn(void*)
	{
		initializeConsole();
		while (true)
		{
			std::unique_ptr<char, decltype(&linenoiseFree)> line(linenoise("esp8266-rts> "), linenoiseFree);
			if (!line)
				continue;

			linenoiseHistoryAdd(line.get());

			int ret;
			esp_err_t err = esp_console_run(line.get(), &ret);

			switch (err)
			{
			case ESP_ERR_NOT_FOUND:
				puts("Unrecognized command");
				break;

			case ESP_ERR_INVALID_ARG:
				// command supposedly empty - do nothing
				break;

			case ESP_OK:
				if (ret != ESP_OK)
					printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(err));
				break;

			default:
				printf("Internal error: %s\n", esp_err_to_name(err));
				break;
			}
		}
	}
}

namespace serial
{
	void init()
	{
		// from the console example
		/* Disable buffering on stdin */
		setvbuf(stdin, NULL, _IONBF, 0);

		/* Minicom, screen, idf_monitor send CR when ENTER key is pressed */
		esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
		/* Move the caret to the beginning of the next line on '\n' */
		esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

		/* Configure UART. Note that REF_TICK is used so that the baud rate remains
		* correct while APB frequency is changing in light sleep mode.
		*/
		uart_config_t uart_config = {};

		uart_config.baud_rate = CONFIG_CONSOLE_UART_BAUDRATE;
		uart_config.data_bits = UART_DATA_8_BITS;
		uart_config.parity = UART_PARITY_DISABLE;
		uart_config.stop_bits = UART_STOP_BITS_1;

		const uart_port_t PORT = static_cast<uart_port_t>(CONFIG_CONSOLE_UART_NUM);
		ESP_ERROR_CHECK(uart_param_config(PORT, &uart_config));

		/* Install UART driver for interrupt-driven reads and writes */
		ESP_ERROR_CHECK(uart_driver_install(PORT, 256, 0, 0, nullptr, 0));

		/* Tell VFS to use UART driver */
		esp_vfs_dev_uart_use_driver(CONFIG_CONSOLE_UART_NUM);
	}

	void start(UBaseType_t priority)
	{
		xTaskCreate(serialTaskFn, "serialTask", 2*1024, nullptr, priority, nullptr);
	}
}
