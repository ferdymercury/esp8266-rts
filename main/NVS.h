/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_NVS_H
#define ESP8266_RTS_NVS_H

#include <cstddef>
#include <cstdint>

namespace nvs
{
	void init();

	void getStr(const char * key, char * value, size_t maxLen);

	template<size_t N>
	void getStr(const char * key, char (&value)[N])
	{
		getStr(key, value, N);
	}

	void setStr(const char * key, char * value);

	template<typename T>
	void get(const char * key, T & value) = delete;

	void get(const char * key, uint16_t & value);
	void get(const char * key, int16_t & value);

	template<typename T>
	void set(const char * key, T value) = delete;

	void set(const char * key, uint16_t value);
	void set(const char * key, int16_t value);

	void commit();
}

#endif // ESP8266_RTS_NVS_H
