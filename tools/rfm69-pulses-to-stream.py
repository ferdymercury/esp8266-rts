#!/usr/bin/env python3
#
# Convert pulses of given length with given bitrate to a series of bytes that
# can be broadcast by RFM69 in OOK encoding.
import sys
import argparse

# RFM69 F_{OSC} frequency
FXOSC = 32e6 # 32 MHz

# convert from bitrate coefficient as used by the RFM69 to actual bitrate (bits/s)
def rfm69BitrateCoeffToBitrate(br):
	return FXOSC / br

parser = argparse.ArgumentParser()
parser.add_argument('bitrateCoeff', help='RFM69 bitrate coefficient', type=int)
parser.add_argument('padToBytes', help='byte-alignment: left mean pad by 0s from left to get a multiple of 8 bits, right pads from right')
parser.add_argument('periods', help='pulse periods, assuming 1st value is for 1, second for 0, thirds for 1 etc.', nargs='*', type=float)
args = parser.parse_args()

if args.padToBytes == 'left':
	padFromLeft = True
elif args.padToBytes == 'right':
	padFromLeft = False
else:
	print('Align argument must be one of "left" or "right".')
	sys.exit(1)

bitrateCoeff = args.bitrateCoeff
period = 1 / rfm69BitrateCoeffToBitrate(bitrateCoeff)
periods = args.periods

print(f'RFM69 bitrate coefficient: {bitrateCoeff}')
print(f'symbol period: {round(period*1e6, 3)}µs')
print('pulse periods:', end='')
for i in range(len(periods)):
	p = periods[i]
	print(f' {round(p * 1e6, 3)}µs[{(i+1) % 2}]', end='')
print()

bits = []
for i in range(len(periods)):
	p = periods[i]
	value = (i+1) % 2
	count = round(p / period)
	for j in range(count):
		bits.append(value)

print('raw bits:', bits)
print(f'raw bits count: {len(bits)}')

# left-pad by zeros to a multiple of 8
padBits = (8 - len(bits) % 8) % 8
if padFromLeft:
	insertIdx = 0
else:
	insertIdx = len(bits)

for i in range(padBits):
	bits.insert(insertIdx, 0)

print(f'adding {padBits} padding 0 bits from {args.padToBytes}:', bits)

print('padded bytes: ', end='')
for i in range(len(bits) // 8):
	index = 8 * i
	byte = 0
	for j in range(8):
		byte = byte * 2 + bits[index + j]

	if (i != 0):
		print(', ', end='')
	print('0x{:02x}'.format(byte), end='')
print()
