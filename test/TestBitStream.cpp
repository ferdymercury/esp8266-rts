/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <cstdint>

#include "../main/radio/BitStream.h"

BOOST_AUTO_TEST_CASE(TestBitStream_nop)
{
	uint8_t buffer[10];
	BitStream bs(buffer, 1);
	BOOST_TEST(bs.getCurrentEnd() == buffer);
}

BOOST_AUTO_TEST_CASE(TestBitStream_simple)
{
	uint8_t buffer[10];
	BitStream bs(buffer, 1);

	bs << 1 << 1 << 1 << 0 << 1 << 1 << 0 << 0;

	BOOST_TEST(bs.getCurrentEnd() == buffer + 1);
	BOOST_TEST(buffer[0] == 0b11101100);
}

BOOST_AUTO_TEST_CASE(TestBitStream_simplePartialByte)
{
	uint8_t buffer[10];
	BitStream bs(buffer, 1);

	bs << 1 << 1 << 1 << 0 << 1 << 1 << 0 << 0 << 1 << 0 << 1;

	BOOST_TEST(bs.getCurrentEnd() == buffer + 2);
	BOOST_TEST(buffer[0] == 0b11101100);
	BOOST_TEST(buffer[1] == 0b10100000);
}

BOOST_AUTO_TEST_CASE(TestBitStream_clearZeros)
{
	uint8_t buffer[5] =
	{
		0xff, 0xff, 0xff, 0xff, 0xff
	};

	BitStream bs(buffer, 1);

	bs << 1 << 1 << 1 << 0 << 1 << 1 << 0 << 0 << 1 << 0 << 1;

	BOOST_TEST(bs.getCurrentEnd() == buffer + 2);
	BOOST_TEST(buffer[0] == 0b11101100);
	BOOST_TEST(buffer[1] == 0b10100000);
	BOOST_TEST(buffer[2] == 0xff);
	BOOST_TEST(buffer[3] == 0xff);
	BOOST_TEST(buffer[4] == 0xff);
}

BOOST_AUTO_TEST_CASE(TestBitStream_x2)
{
	uint8_t buffer[10];
	BitStream bs(buffer, 2);

	bs << 1 << 1 << 1 << 0 << 1;

	BOOST_TEST(bs.getCurrentEnd() == buffer + 2);
	BOOST_TEST(buffer[0] == 0b11111100);
	BOOST_TEST(buffer[1] == 0b11000000);
}

BOOST_AUTO_TEST_CASE(TestBitStream_x3)
{
	uint8_t buffer[10];
	BitStream bs(buffer, 3);

	bs << 1 << 1 << 1 << 0 << 1;

	BOOST_TEST(bs.getCurrentEnd() == buffer + 2);
	BOOST_TEST(buffer[0] == 0b11111111);
	BOOST_TEST(buffer[1] == 0b10001110);
}

BOOST_AUTO_TEST_CASE(TestBitStream_overflow)
{
	constexpr uint8_t CANARY_MAGIC = 0;
	uint8_t buffer[1];
	volatile uint8_t canary = CANARY_MAGIC;

	BitStream bs(buffer, 3);
	bs << 1 << 1 << 1 << 1 << 1 << 1 << 1 << 1 << 1;

	BOOST_TEST(buffer[0] == 0xff);
	BOOST_TEST(canary == CANARY_MAGIC);
}
