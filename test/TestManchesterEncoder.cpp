/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <vector>

#include "../main/radio/ManchesterEncoder.h"

namespace
{
	class FakeBitStream
	{
	public:
		FakeBitStream()
		{}

		const std::vector<bool> & getBits() const
		{
			return m_bits;
		}

		FakeBitStream & operator<<(bool b)
		{
			m_bits.push_back(b);
			return *this;
		}

	private:
		std::vector<bool> m_bits;
	};
}

BOOST_AUTO_TEST_CASE(TestManchesterEncoder_nop)
{
	FakeBitStream bs;
	ManchesterEncoder<FakeBitStream> encoder(bs);

	encoder << 0b10110010;

	const std::vector<bool> expectedBits{
		0, 1,
		1, 0,
		0, 1,
		0, 1,
		1, 0,
		1, 0,
		0, 1,
		1, 0
	};

	const auto & bits = bs.getBits();
	BOOST_TEST(bits.size() == 2 * 8);
	BOOST_TEST(bits == expectedBits);
}
